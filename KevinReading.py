# Import all the things

import random as rd
from datetime import datetime
import discord
import asyncio
from git import Repo
from os import getcwd
from os import system

# Debugging functionally for decoding crashes

DEBUGGING = True

def debugging(dbstr):
	if DEBUGGING:
		print(str(datetime.today()) + " DEBUGGING: " + dbstr)

# Method that returns a random kevin reading (if index is rand)
# setting index to anything other than rand will return the first passage the cpu finds that contains the words that index is set to
# if it cant find any phrase, it will return a string "could not find" + index

def ReadKevin(index='rand'):

	# Replacing unholy words with holy ones

	with open("bible.txt", "r") as bible:

		bible = bible.read()
		bible = bible.replace("\t", " ")
		bible = bible.replace(";", "")
		bible = bible.replace(" Gods", " Teachers")
		bible = bible.replace(" LORD", " Kevin")
		bible = bible.replace(" Lord", " Kevin")
		bible = bible.replace(" he ", " Kevin ")
		bible = bible.replace(" he.", " Kevin.")
		bible = bible.replace(" he!", " Kevin!")
		bible = bible.replace(" he,", " Kevin,")
		bible = bible.replace(" he?", " Kevin?")
		bible = bible.replace(" He ", " Kevin ")
		bible = bible.replace(" Thy", " Kevin's")
		bible = bible.replace(" thy", " Kevin's")
		bible = bible.replace(" Jacob", " Griffin")
		bible = bible.replace(" they", " Kevin and his Holy Civ Class")
		bible = bible.replace(" I", " Kevin")
		bible = bible.replace(" his", " Kevin's")
		bible = bible.replace(" them", " the students")
		bible = bible.replace(" our", " the followers of Kevin's")
		bible = bible.replace(" the Kevin", " Kevin")
		bible = bible.replace(" God", " Kevin")
		bible = bible.replace("Psalms", "The Book of Kevin")
		bible = bible.replace(" Zion", " Kevin's bountiful classroom")
		bible = bible.split("\n")

		# Dealing with an index value set to anything besides "rand"
		# Trying to find a passage containing the value of index

		if index == 'rand':
			indexr = rd.randint(0, len(bible))
			return bible[indexr]

		try:
			int(index.replace(":", ""))
			for phrase in bible:
				if index in phrase[18:25]:
					return phrase
		except:
			return 'Please ask me to read a page number.'

		return "Could not find " + index

# Discord Bot

client = discord.Client()

# When the Bot is ready

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

# When the bot sees a message sent

@client.event
async def on_message(message):
	if message.content.startswith('worship kevin') or message.content.startswith('Worship kevin') or message.content.startswith('Worship Kevin') or message.content.startswith('worship Kevin'):
		await client.send_message(message.channel, ReadKevin())
		debugging("sent kevin message on channel " + str(message.channel))

	elif message.content[:22] == 'read the book of kevin':
		if message.content[23:] != '':
			await client.send_message(message.channel, ReadKevin(index=message.content[23:]))


# A background task that sends a random kevin passage every 24 hours at 8:00 AM

async def my_background_task():
	await client.wait_until_ready()
	channel = discord.Object(id='441656428147638272')
	while not client.is_closed:
		ahoy = datetime.now()
		await asyncio.sleep(30)
		if ahoy.hour == 8: # 8:00am
			await client.send_message(channel, ReadKevin())
			debugging("sent daily kevin reading")
			await asyncio.sleep(60*60*23.9) #1 min * 60 mins in an hour * 24(ish) hours in a day
			debugging("sleeping for >24h")
		else:
			debugging("checking time every 30 seconds")

# Background Task that automatically tries to update and restart the bot

async def gitupdatechecker():
	await client.wait_until_ready()
	repo = Repo(getcwd())
	Git = repo.git()
	while not client.is_closed:
		await asyncio.sleep(60)		
		if Git.pull() != "Already up-to-date.":
			print("checked for updates, new version found, spawning new prosess...")
			print("")
			system("python3 KevinReading.py")
		else:
			debugging("checked for updates, none found")

# Running the bot and adding the background tasks to the event loop (thank kevin for asyncio)

client.loop.create_task(my_background_task())
client.loop.create_task(gitupdatechecker())
client.run("MzY1NTQ0ODAxOTU0OTU1Mjc3.DLf3tg.mCsQ7yocmg9F78mOJA-SoFftzFg")
